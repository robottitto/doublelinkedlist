package test.java.utils;

import main.java.utils.DoubleLinkedList;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class DoubleLinkedListTest {

    private static DoubleLinkedList list;

    @BeforeAll
    static void beforeAll() {
        list = new DoubleLinkedList();
        list.addFirst("item");
    }

    @Test
    @Order(1)
    void testAddFirst() {
        list.addFirst("item1");
        assertEquals("item1", list.getFirst());
    }

    @Test
    @Order(2)
    void testRemoveFirst() {
        list.removeFirst();
        assertNotEquals("item1", list.getFirst());
    }

    @Test
    @Order(3)
    void testAddLast() {
        list.addLast("item5");
        assertEquals("item5", list.getLast());
    }

    @Test
    @Order(4)
    void testRemoveLast() {
        list.removeLast();
        assertNotEquals("item5", list.getLast());
    }

    @Test
    @Order(5)
    void testIsEmpty() {
        list.removeFirst();
        assertTrue(list.isEmpty());
    }

}