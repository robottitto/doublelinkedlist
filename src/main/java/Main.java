package main.java;

import main.java.utils.DoubleLinkedList;

public class Main {

    public static void main(String[] args) {
        DoubleLinkedList list = new DoubleLinkedList();

        System.out.println(list.addFirst("item4") + " was added to list");
        System.out.println(list.addFirst("item3") + " was added to list");
        System.out.println(list.addFirst("item2") + " was added to list");
        System.out.println(list.addFirst("item1") + " was added to list");
        System.out.println(list.addLast("item5") + " was added to list");

        System.out.println("List: " + list.toString());

        System.out.println(list.removeFirst() + " was removed from list");
        System.out.println(list.removeFirst() + " was removed from list");
        System.out.println(list.removeLast() + " was removed from list");
        System.out.println(list.removeLast() + " was removed from list");

        System.out.println("List: " + list.toString());
    }

}
