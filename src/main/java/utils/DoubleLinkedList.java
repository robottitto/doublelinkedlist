package main.java.utils;

import java.util.NoSuchElementException;

public class DoubleLinkedList<T> {

    private Node first = null;
    private Node last = null;

    public T addFirst(T item) {
        Node newItem = new Node(item);

        if (this.isEmpty()) {
            this.first = newItem;
            this.last = newItem;
        } else {
            this.first.prev = newItem;
            newItem.next = this.first;
            this.first = newItem;
        }
        return (T) newItem.item;
    }

    public T getFirst() {
        if (this.isEmpty()) {
            throw new NoSuchElementException();
        }
        return (T) this.first.item;
    }

    public T removeFirst() {
        if (this.isEmpty()) {
            throw new NoSuchElementException();
        } else {
            T removedItem = (T) this.first.item;
            if (this.first.next == null) {
                this.first = null;
                this.last = null;
            } else {
                this.first = this.first.next;
                this.first.prev = null;
            }
            return removedItem;
        }
    }

    public T addLast(T item) {
        Node newItem = new Node(item);

        if (this.isEmpty()) {
            this.last = newItem;
            this.first = newItem;
        } else {
            this.last.next = newItem;
            newItem.prev = this.last;
            this.last = newItem;
        }
        return (T) newItem.item;
    }

    public T getLast() {
        if (this.isEmpty()) {
            throw new NoSuchElementException();
        }
        return (T) this.last.item;
    }

    public T removeLast() {
        if (this.isEmpty()) {
            throw new NoSuchElementException();
        } else {
            T removedItem = (T) this.last;
            if (this.first.next == null) {
                this.first = null;
                this.last = null;
            } else {
                this.last = this.last.prev;
                this.last.next = null;
            }
            return removedItem;
        }
    }

    public boolean isEmpty() {
        return this.first == null;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for(Node current = this.first; current != null; current = current.next) {
            sb.append(current);
            sb.append(" ");
        }
        return sb.toString();
    }

    private class Node<T> {

        public T item;
        public Node next;
        public Node prev;

        public Node(T item) {
            this.item = item;
        }

        @Override
        public String toString() {
            return item.toString();
        }

    }
}
